# FROM kontr2/testing-ktdk-runner
FROM kontr2/ktdk-cpp-runner

MAINTAINER peter.stanko0@gmail.com

ADD . /workspace/test_files
WORKDIR /workspace/test_files

ONBUILD COPY Pipfile Pipfile
ONBUILD COPY Pipfile.lock Pipfile.lock

RUN pipenv install --system

ENTRYPOINT ["time", "ktdk", "execute"]





