= Hello test files

Simple test files repository for the hello project

== Install dependencies

[source, shell]
----
$ pipenv install
----

== Run ktdk command

[source, shell]
----
$ ktdk --help
----

== Test the test repo using the `kontrctl`

To use this, use the `dev` config build. By default, it works with the `docker-compose` platform template.

[source, shell]
----
$  kontrctl login && kontrctl submit -c testcourse1 -p hw01 -u https://gitlab.fi.muni.cz/grp-kontr2/testing/test-repo.git -D hw01 
----